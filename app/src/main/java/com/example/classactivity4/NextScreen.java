package com.example.classactivity4;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class NextScreen extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.next_screen);
        Bundle bundle = getIntent().getExtras();
        String name= bundle.getString("name");
        String address= bundle.getString("address");
        String phone= bundle.getString("phone");
        TextView editTextPhone = findViewById(R.id.textViewName);
        editTextPhone.setText("Name: "+name);
        TextView textViewAddress = findViewById(R.id.textViewAddress);
        textViewAddress.setText("Address: "+address);
        TextView textViewNumber = findViewById(R.id.textViewNumber);
        textViewNumber.setText("Phone: "+phone);
    }

    public void onClickBackButton(View view) {
        Intent intent = new Intent(com.example.classactivity4.NextScreen.this, MainActivity.class);
        startActivity(intent);
    }
}