package com.example.classactivity4;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private Button button;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }


    public void onClickSubmitButton(View view) {
        EditText editTextPersonName = findViewById(R.id.editTextPersonName);
        String name = editTextPersonName.getText().toString();
        EditText editTextAddress = findViewById(R.id.editTextAddress);
        String address = editTextAddress.getText().toString();
        EditText editTextPhone = findViewById(R.id.editTextPhone);
        String phone = editTextPhone.getText().toString();
        Intent intent = new Intent(com.example.classactivity4.MainActivity.this, NextScreen.class);
        Bundle bundle = new Bundle();
        bundle.putString("name", name);
        bundle.putString("address", address);
        bundle.putString("phone", phone);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}