package com.example.classactivity4;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;


public class SplashScreen extends Activity {

    Handler handler;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_background);
        handler=new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                Intent intent=new Intent(com.example.classactivity4.SplashScreen.this, com.example.classactivity4.MainActivity.class);
                startActivity(intent);
                finish();
            }
        },2000);

    }
}
